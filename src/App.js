import "./App.css";

function App() {
  const weekdays = ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"];
  return (
    <div className="App">
      <header className="App-header">W13 Weekdays in table</header>
      <table className="App-table">
        <tr>
          <th>Weekday</th>
        </tr>
        {weekdays.map((day) => (
          <tr>
            <td>{day}</td>
          </tr>
        ))}
      </table>
    </div>
  );
}

export default App;
